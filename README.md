**================
PSSE to MATPOWER
================**

**Description:** parses data from Siemens PSSE files into MATPOWER format

**Usage:** python parser.py FILENAME (OUTPUTNAME)

The script should work well with PSSE files **version 33**.


**Important:** When you save the .raw file from PSSE, there is a section of the
dialog that says *Include*. You should uncheck **everything**.


**Features:**

[x] Two-winding transformers

[ ] Three-winding transformers (Untested)

[x] Switched shunt

[x] Remote bus

[x] Impedance loads

[x] Current loads

[x] Line Shunts


**TODO**:
The script crashes when blank lines are found among the data. I'll fix this soon. In the meanwhile you can open the raw datafile with Vi and type:

:g/^$/d

This will get rid of the blank lines.

Also, the first 3 first lines in the raw datafile must be similar to this:

0,   100.00, 33, 0, 1, 60.00     / PSS(R)E UNIVERSITY-33.2    THU, APR 04 2013  19:54

PSS(R)E PROGRAM APPLICATION GUIDE EXAMPLE

BASE CASE INCLUDING SEQUENCE DATA

The script looks for "BASE CASE" and then starts to parse data. This is shameful and will be done in a proper way... later.
