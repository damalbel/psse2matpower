from math import fabs, pow, sqrt


global int2ext
int2ext = {}

def busd(bus,bus_mat):
    """ Return bus position in structure"""
    return int2ext[bus]

#    for i in range(len(bus_mat)):
#        if bus_mat[i][0] == bus:
#            return i
            
def max_bus(bus_mat):
    """Returns max bus number"""
    bmax = 0
    for i in range(len(bus_mat)):
        if bus_mat[i][0] > bmax:
            bmax = bus_mat[i][0]
    
    return bmax
    
def search_gen(gen_mat, bus):

    vecino = 0
    for i in range(len(gen_mat)):
        if gen_mat[i][0] == bus:
            vecino = i

    return vecino

def build_imp(table):

    output = {}
    
    for i in range(len(table)):
        
        size = len(table[i])
        p = 1
        data =[]
        for j in range((size-1)/2):
            data.append([table[i][p], table[i][p+1]])
            p = p + 2
        
        output[table[i][0]] = data
        
    return output
        
def impedance_correction(table, angle):
    """Returns correction factor"""
    """Nota: tratar de buscar un algoritmo eficiente"""
    
    for i in range(len(table)):
        if table[i][0] == angle: return table[i][1]

    bot = [-999, 0]
    top = [999, 0]
    
    
    for i in range(len(table)):
        if (table[i][0] > bot[0]) & (table[i][0] < angle):
            bot = table[i]

    for i in range(len(table)):
        if table[i][0] > angle:
            top = table[i]
            break
    if top[1] == bot[1]:
        return top[1]
    else:
        k = (top[0]-bot[0])/(top[1]-bot[1])
        x = (k*top[1] - top[0] + angle) / k
        return x

def set_type(line):
    """ Set datatypes in a list"""
    
    #This is embarrasing but it works. Just
    #using exceptions to try to enforce a 
    #datatype.
    
    
    for i in range(len(line)):
        try:
            line[i] = int(line[i])
        except:
            try:
                line[i] = float(line[i])
            except:
                line[i] = line[i]
    return line

def single_names(string):
    """Removes whitespaces in PSSE strings"""
    
    init = string.find('\'')
        
    if init != -1:
        end = string.find('\'', init+1)
    else:
        return string # no strings here
        
    curated_name = string[init:end].replace(' ','')
    
    return string[:init] + curated_name + string[end:]

#June-2014 KenMcK contribution:
def polish(data):
   """ Separate the values within the string into
       different elements in a list        """

   for i in range(len(data)):
       data[i] = data[i].replace(' ','')
       data[i] = data[i].replace('\'','')
       data[i] = data[i].replace(',',' ')
       data[i] = " ".join(data[i].split()).split()
       data[i] = set_type(data[i])

   return data

def create_bus(psse_bus, psse_load, psse_fshunt, psse_sshunt, psse_branch):

    """Creates bus structure"""

    bus_mat = []

    #June-2014 KenMcK added voltages [9] and [10]
    #initial structure from pss_bus file
    for i in range(len(psse_bus)):
        try:
            bus_mat.append([psse_bus[i][0], psse_bus[i][3], 0, 0, 0, 0,
            psse_bus[i][4], psse_bus[i][7], psse_bus[i][8], psse_bus[i][2],
            psse_bus[i][5], psse_bus[i][9], psse_bus[i][10]])

            int2ext[psse_bus[i][0]] = i

        except:
            print "I/O Error"
            print psse_bus[i-1]
            print psse_bus[i]
            
    #add fixed shunt
    for i in range(len(psse_fshunt)):
        if psse_fshunt[i][2] == 1:
            busn = busd(psse_fshunt[i][0], bus_mat) #lookup bus position
            
            bus_mat[busn][4] += psse_fshunt[i][3]
            bus_mat[busn][5] += psse_fshunt[i][4]
            
    #adding the loads present at psse_load
    #we check if the load is active and then we
    #add the value to the correspondent cell in the bus structure.
    for i in range(len(psse_load)):
        if psse_load[i][2] == 1:

            busn = busd(psse_load[i][0], bus_mat) #lookup bus position
            
            # PQ LOAD
            bus_mat[busn][2] += psse_load[i][5]
            bus_mat[busn][3] += psse_load[i][6]
            # Z LOAD
            # PSSE Z load is given in MW at Vref
            # we can add it directly to the bus shunt
            bus_mat[busn][4] += psse_load[i][9]
            bus_mat[busn][5] += psse_load[i][10]
            
            #lets try this for now
            #bus_mat[busn][2] += psse_load[i][9]
            #bus_mat[busn][3] += psse_load[i][10]
            
            # I LOAD
            # This should be P=P0*(V/V0) but we are not
            # supposed to know V until doing the PFlow.
            # Lets use V = bus voltage and V0 = 1 pu
            bus_mat[busn][2] += (psse_load[i][7]*bus_mat[busn][7])
            bus_mat[busn][3] += (psse_load[i][8]*bus_mat[busn][7])
            
    #add switched shunts (approximation)
    
    for i in range(len(psse_sshunt)):
        busn = busd(psse_sshunt[i][0], bus_mat) #lookup bus position
        bus_mat[busn][5] += psse_sshunt[i][8]
        
        
        
    #add line shunts
    
    for i in range(len(psse_branch)):
        fr_bus = busd(psse_branch[i][0], bus_mat)
        to_bus = busd(psse_branch[i][1], bus_mat)
        
        bus_mat[fr_bus][4] += psse_branch[i][9]*100.0
        bus_mat[fr_bus][5] += psse_branch[i][10]*100.0 
        bus_mat[to_bus][4] += psse_branch[i][11]*100.0 
        bus_mat[to_bus][5] += psse_branch[i][12]*100.0       

    return bus_mat
    
def create_gen(psse_gen, mat_bus):
    
    gen_mat =[]
    
    #initial structure
    for i in range(len(psse_gen)):
    
        busn = busd(psse_gen[i][0], mat_bus)
        #vecino = search_gen(gen_mat, psse_gen[i][0]) 
    
        if (psse_gen[i][14] == 1) & (mat_bus[busn][1] != 1): #only if there is a gen
    
            #Remote bus voltage
            if psse_gen[i][7] != 0:
                volt = mat_bus[busn][7]
            else:
                volt = psse_gen[i][6]
                
            #if vecino == 0:
                
            gen_mat.append([psse_gen[i][0], psse_gen[i][2], psse_gen[i][3], psse_gen[i][4], psse_gen[i][5],
                    volt, psse_gen[i][8], psse_gen[i][14], psse_gen[i][16], psse_gen[i][17]])
                    
            #else:
            #    gen_mat[vecino][1] += psse_gen[i][2]
            #    gen_mat[vecino][2] += psse_gen[i][3]
            #    gen_mat[vecino][3] += psse_gen[i][4]
            #    gen_mat[vecino][4] += psse_gen[i][5]
            #    gen_mat[vecino][6] += psse_gen[i][8]
            #    gen_mat[vecino][8] += psse_gen[i][16]
            #    gen_mat[vecino][9] += psse_gen[i][17]

    return gen_mat, mat_bus
    
    
def create_branch(psse_branch, psse_trans, mat_bus, impedance_table):

    branch_mat = []
    
    #initial structure from psse_branch
    for i in range(len(psse_branch)):
        
        branch_mat.append([psse_branch[i][0], fabs(psse_branch[i][1]), psse_branch[i][3], 
						psse_branch[i][4], psse_branch[i][5], psse_branch[i][6], psse_branch[i][7], 
						psse_branch[i][8], 0, 0, psse_branch[i][13]])

    
    registers = len(psse_trans)
    p = 0 # position or register
    new_bus = 10000000
    while p < registers :
    
    #lets see if this transformer is 2 or 3 windings
    
        if psse_trans[p][2] == 0:
            #This is a two winding
			# We make modifications to the impedances depending on the code
			

            if psse_trans[p][4] == 2: #CW
                volt2 = psse_trans[p+3][0]/mat_bus[busd(psse_trans[p][1], 
			            mat_bus)][9]
                volt1 = psse_trans[p+2][0]/mat_bus[busd(psse_trans[p][0], 
			            mat_bus)][9]
			
            else:
                volt1 = psse_trans[p+2][0] 
                volt2 = psse_trans[p+3][0]
            
            
            if psse_trans[p][5] == 1: #CZ
            
                r12 = psse_trans[p+1][0] * pow(volt2, 2)
                x12 = psse_trans[p+1][1] * pow(volt2, 2)
			
            if psse_trans[p][5] == 2:
            
                r12 = psse_trans[p+1][0] * (100.0/psse_trans[p+1][2])*pow(volt2, 2)
                x12 = psse_trans[p+1][1] * (100.0/psse_trans[p+1][2])*pow(volt2, 2)
				
            elif psse_trans[p][5] == 3:
            
			    r12 = (psse_trans[p+1][0] / pow(10, 6)) / psse_trans[p+1][2]
			    x12 = sqrt(pow(psse_trans[p+1][1], 2) - pow(r12, 2))
			    r12 = r12 * (100.0/psse_trans[p+1][2]) * pow(volt2, 2)
			    x12 = x12 * (100.0/psse_trans[p+1][2]) * pow(volt2, 2)

            tap = (volt1 / volt2)
            
            
            #correction
            if (psse_trans[p+2][2] != 0.0) & (psse_trans[p+2][13] != 0):

                correction = impedance_correction(impedance_table[psse_trans[p+2][13]], psse_trans[p+2][2])

                r12 = correction*r12
                x12 = correction*x12


            branch_mat.append([psse_trans[p][0], psse_trans[p][1], r12, x12, 0,
            	psse_trans[p+2][3], psse_trans[p+2][4], psse_trans[p+2][5], tap, 
            	psse_trans[p+2][2], psse_trans[p][10]])
			
            
            #Magnetizing impedance
            fr_bus = busd(psse_trans[p][0], mat_bus)
            if psse_trans[p][6] == 1:
                mat_bus[fr_bus][4] += psse_trans[p][7]*100.0;
                mat_bus[fr_bus][5] += psse_trans[p][8]*100.0;   
            else:
                #deb(volt1)
                #nominal voltage
                if psse_trans[p+2][1] < 0.1:
                    nomv = 1
                else:
                    nomv = psse_trans[p+2][1]/mat_bus[busd(psse_trans[p][0], 
			            mat_bus)][9]
                
                plpu = psse_trans[p][7]/(100*pow(10, 6))
                iepu = psse_trans[p][8]*(psse_trans[p+1][2]/100.0)*(1/nomv)
                
                gmpu = plpu/pow(nomv, 2)
                igpu = nomv*gmpu
                ibpu = sqrt(pow(iepu, 2) - pow(igpu, 2))
                bmpu = -(ibpu/nomv)
                
                mat_bus[fr_bus][4] += gmpu*100.0;
                mat_bus[fr_bus][5] += bmpu*100.0;
                

            p += 4 #next register            

        else:
            #This is a three winding, we create a virtual bus and proceed
            #Voltages are taken of the 3-winding hidden star bus
            
            #new_bus = max_bus(mat_bus) + 1
            new_bus = new_bus + 1

            new_bus_volt = mat_bus[busd(psse_trans[p][0], mat_bus)][9]
            
            mat_bus.append([new_bus, 2, 0, 0, 0, 0, 
            	1, psse_trans[p+1][9], psse_trans[p+1][10], new_bus_volt, 1, 1.1, 0.9])
           
            
            # First the winding base voltages are converted to per-unit if they arent
            
            if psse_trans[p][4] == 2: #CW
                volt1 = psse_trans[p+2][0]/mat_bus[busd(psse_trans[p][0], 
			            mat_bus)][9]
                volt2 = psse_trans[p+3][0]/mat_bus[busd(psse_trans[p][1], 
			            mat_bus)][9]
                volt3 = psse_trans[p+4][0]/mat_bus[busd(psse_trans[p][2], 
			            mat_bus)][9]
            else: 
                volt1 = psse_trans[p+2][0]
                volt2 = psse_trans[p+3][0]
                volt3 = psse_trans[p+4][0]
            
            #then the impedances are converted to a proper value
            
            
            if psse_trans[p][5] == 1:
            
                r12 = psse_trans[p+1][0]
                r23 = psse_trans[p+1][3]
                r13 = psse_trans[p+1][6]
                
                x12 = psse_trans[p+1][1]
                x23 = psse_trans[p+1][4]
                x13 = psse_trans[p+1][7]
                
            elif psse_trans[p][5] == 2:
            
                r12 = psse_trans[p+1][0]*(100.0/psse_trans[p+1][2])
                r23 = psse_trans[p+1][3]*(100.0/psse_trans[p+1][5])
                r13 = psse_trans[p+1][6]*(100.0/psse_trans[p+1][8])
                
                x12 = psse_trans[p+1][1]*(100.0/psse_trans[p+1][2])
                x23 = psse_trans[p+1][4]*(100.0/psse_trans[p+1][5])
                x13 = psse_trans[p+1][7]*(100.0/psse_trans[p+1][8])
						
            else:
				r12 = (psse_trans[p+1][0] / pow(10,6)) / psse_trans[p+1][2]
				r23 = (psse_trans[p+1][3] / pow(10,6)) / psse_trans[p+1][5]
				r13 = (psse_trans[p+1][6] / pow(10,6)) / psse_trans[p+1][8]
				x12 = sqrt(pow(psse_trans[p+1][1], 2) - pow(r12, 2))
				x23 = sqrt(pow(psse_trans[p+1][4], 2) - pow(r23, 2))
				x13 = sqrt(pow(psse_trans[p+1][7], 2) - pow(r13, 2))
				
				r12 = r12 * (100.0/psse_trans[p+1][2])
				x12 = x12 * (100.0/psse_trans[p+1][2])
				r23 = r23 * (100.0/psse_trans[p+1][5])
				x23 = x23 * (100.0/psse_trans[p+1][5])
				r13 = r13 * (100.0/psse_trans[p+1][8])
				x13 = x13 * (100.0/psse_trans[p+1][8])
							

            r1 = 0.5 * (r12 + r13 - r23)
            r2 = 0.5 * (r12 - r13 + r23)
            r3 = 0.5 * (r13 + r23 - r12)
            
            x1 = 0.5 * (x12 + x13 - x23)
            x2 = 0.5 * (x12 - x13 + x23)
            x3 = 0.5 * (x13 + x23 - x12)
            
            tap1 = volt1
            tap2 = volt2
            tap3 = volt3
            
            #The initial status (service) of the transformer.

            if psse_trans[p][10] == 0: s1, s2, s3 = 0, 0, 0
            elif psse_trans[p][10] == 1: s1, s2, s3 = 1, 1, 1
            elif psse_trans[p][10] == 2: s1, s2, s3 = 1, 0, 1
            elif psse_trans[p][10] == 3: s1, s2, s3 = 1, 1, 0
            elif psse_trans[p][10] == 4: s1, s2, s3 = 0, 1, 1
            
            branch_mat.append([psse_trans[p][0], new_bus, r1, x1, 0,
                psse_trans[p+2][3], psse_trans[p+2][4], psse_trans[p+2][5], tap1, psse_trans[p+2][2], 
                s1])
            branch_mat.append([new_bus, psse_trans[p][1], r2, x2, 0,
                psse_trans[p+3][3], psse_trans[p+3][4], psse_trans[p+3][5], tap2, psse_trans[p+3][2], 
                s2])
            branch_mat.append([new_bus, psse_trans[p][2], r3, x3, 0,
                psse_trans[p+4][3], psse_trans[p+4][4], psse_trans[p+4][5], tap3, psse_trans[p+4][2], 
                s3])
                
                
            p += 5

    return branch_mat, mat_bus
    
if __name__ == "__main__":

    caca = "3002 'E. MINE     '  500.0000 1      0.000      0.000    5    5 1.02791   -1.8253    5"
    
    print single_names(caca)
    
    
    
        
