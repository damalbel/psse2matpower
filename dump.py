# This file contains the function to write the data structures to a formated .mat file


def write_all(mat_bus, mat_gen, mat_branch, outputName):
    """ Write the created structures into a matlab-formated file"""
    
    output = open(outputName +'.m','w')
    
    output.write('function mpc = %s\n\n\n\n' % outputName)
    
    output.write('%%% New case for matpower\n\n\n')
    output.write('mpc.version=\'2\';\n\n')
    output.write('mpc.baseMVA = 100.00;\n\n')
    
    ## Print bus matrix
    output.write('mpc.bus = [\n')
    
    
    for i in range(len(mat_bus)):
        output.write("\t%10i\t%1i\t%8.3f\t%8.3f\t%5.3f\t%8.3f\t%3i\t%8.7f\t%8.7f\t%7.2f\t\t%1i\t%3.2f\t%3.2f;\n" 
            % (mat_bus[i][0],mat_bus[i][1],mat_bus[i][2],mat_bus[i][3],
            mat_bus[i][4],mat_bus[i][5],mat_bus[i][6],mat_bus[i][7],
            mat_bus[i][8],mat_bus[i][9],mat_bus[i][10],mat_bus[i][11],
            mat_bus[i][12]))
    
    output.write('];\n\n')
    
    output.write('mpc.gen = [\n')
    
    
    for i in range(len(mat_gen)):
        output.write("\t%5i\t%8.3f\t%8.3f\t%8.3f\t%8.3f\t%5.3f\t%8.3f\t%2i\t%8.3f\t%8.3f;\n"
            % (mat_gen[i][0],mat_gen[i][1],mat_gen[i][2],mat_gen[i][3],
            mat_gen[i][4],mat_gen[i][5],mat_gen[i][6],mat_gen[i][7],
            mat_gen[i][8],mat_gen[i][9]))
    
    
    output.write('];\n\n')
    
    output.write('mpc.branch = [\n')
    
    for i in range(len(mat_branch)):

        output.write("\t%5i\t%5i\t%2.9f\t%2.9f\t%2.9f\t%8.3f\t%8.3f\t%8.3f\t%2.8f\t%3.8f\t%3i\t\t-360\t360;\n"
            % (mat_branch[i][0],mat_branch[i][1],mat_branch[i][2],mat_branch[i][3],
            mat_branch[i][4],mat_branch[i][5],mat_branch[i][6],mat_branch[i][7],
            mat_branch[i][8],mat_branch[i][9],mat_branch[i][10]))
    
    output.write('];\n\n')
    
    output.close()
    
    
    
