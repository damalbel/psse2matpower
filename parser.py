# parser.py converts PSS/E data files into MATPOWER & TS3PH case file.
#
# Usage: python parser.py INPUTFILE OUTPUTFILE
# Output: case.m
#
# Author:   Adrian Maldonado
# Email:    dmaldona@hawk.iit.edu
#
# Date:     02-April-2013


from sys import argv
from functions import *
from dump import *

narg = len(argv)

if narg == 1:
    raise SystemExit('Usage: python parser.py INPUTFILENAEM (OUTPUTFILENAME)')

elif narg == 2:
    script, filename = argv
    output = 'output'

else:
    script, filename, output = argv


#import pycallgraph
#pycallgraph.start_trace()


#Main routine

data = open(filename, 'r').readlines()

bof = 0 #beginning of data
eof = 0 #end of data


# Scan bus data
while ('BASE CASE' in data[bof]) == False :
    bof += 1
eof = bof

while ('END OF BUS DATA' in data[eof]) == False :
    eof += 1


psse_bus = polish(data[bof+1:eof])
bof = eof



# Scan load data
while ('END OF LOAD DATA' in data[eof]) == False :
    eof += 1

psse_load = polish(data[bof+1:eof])
bof = eof

# Scan load data
while ('END OF FIXED SHUNT DATA' in data[eof]) == False :
    eof += 1

psse_fshunt = polish(data[bof+1:eof])
bof = eof

# Scan gen data
while ('END OF GENERATOR DATA' in data[eof]) == False :
    eof += 1

psse_gen = polish(data[bof+1:eof])
bof = eof

# Scan branch data
while ('END OF BRANCH DATA' in data[eof]) == False :
    eof += 1

psse_branch = polish(data[bof+1:eof])
bof = eof

# Scan trans data
while ('END OF TRANSFORMER DATA' in data[eof]) == False :
    eof += 1

psse_tran = polish(data[bof+1:eof])
bof = eof

# Scan switched shunt data
while ('BEGIN IMPEDANCE CORRECTION DATA' in data[bof]) == False:
    bof += 1
eof = bof

while ('END OF IMPEDANCE CORRECTION DATA' in data[eof]) == False:
    eof += 1

psse_table = polish(data[bof+1:eof])


# Scan switched shunt data
while ('BEGIN SWITCHED SHUNT DATA' in data[bof]) == False:
    bof += 1
eof = bof

while ('END OF SWITCHED SHUNT DATA' in data[eof]) == False:
    eof += 1

psse_sshunt = polish(data[bof+1:eof])



# create structures following matpower pattern

impedance_table = build_imp(psse_table)
mat_bus = create_bus(psse_bus, psse_load, psse_fshunt, psse_sshunt, psse_branch)
mat_gen, mat_bus = create_gen(psse_gen, mat_bus)
mat_branch, mat_bus = create_branch(psse_branch, psse_tran, mat_bus, impedance_table)


# print those structures into a txt file

write_all(mat_bus, mat_gen, mat_branch, output)

#pycallgraph.make_dot_graph('test2.png')
